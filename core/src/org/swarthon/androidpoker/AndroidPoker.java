package org.swarthon.androidpoker;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.utils.DragListener;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;

public class AndroidPoker extends InputAdapter implements ApplicationListener {
    Stage stage;
    Game game;

    InfoObject info;
    
    TextureAtlas buttonsAtlas;
    Skin buttonSkin;
    TextButtonStyle style;

    protected void drawCards(CardObject[] cards, float xpos, float ypos) {
        for(int i = 0; i < cards.length; i++) {
            cards[i].setPosition(xpos + (1.f/2.f * (float)i - 1.f/4.f * (float)cards.length) * CardDesign.CARD_WIDTH / 2.f,
                          ypos - CardDesign.CARD_HEIGHT/4);
            stage.addActor(cards[i]);
        }
    }
    protected void drawPlayers(PlayerObject[] players) {
        int size = game.getPlayers().length;
        float xradius = stage.getWidth()/2.5f;
        float yradius = stage.getHeight()/3f;
        for(int i = 0; i < players.length; i++) {
            float an = 2.f * (float)Math.PI / size * i - (float)Math.PI / 2.f;
            float x = (float)Math.cos(an) * xradius + stage.getWidth()/2.f;
            float y = (float)Math.sin(an) * yradius + stage.getHeight()/2.f;

            players[i].setPosition(x,y);
            stage.addActor(players[i]);
        }
    }
    protected void drawCenter(CardObject[] center) {
        drawCards(center, stage.getWidth()/2.f, stage.getHeight()/2.f);
    }
    
	public void create () {
		stage = new Stage();
		Gdx.input.setInputProcessor(stage);

        final int playerNum = 6;
        game = new Game(playerNum,1000);
        drawCenter(game.getCenter());
        drawPlayers(game.getPlayers());

        info = new InfoObject(game);
        info.setPosition(stage.getWidth() / 4, stage.getHeight()/2 - info.getHeight()/2);
        stage.addActor(info);
        
        buttonsAtlas = new TextureAtlas("buttons/Button.atlas");
        buttonSkin = new Skin();
        buttonSkin.addRegions(buttonsAtlas);

        style = new TextButtonStyle();
        style.up = buttonSkin.getDrawable("normal");
        style.down = buttonSkin.getDrawable("pressed");
        style.font = new BitmapFont();

        TextButton[] buttons = new TextButton[3];

        for(int i = 0; i < buttons.length; i++) {
            TextButton button = new TextButton("", style);
            button.getLabel().setFontScale(3);
            button.setWidth(stage.getWidth()/10);
            button.setHeight(stage.getWidth()/20);
            button.setPosition(stage.getWidth()/2 - button.getWidth()*2,
                               (button.getHeight() + button.getHeight()/10) * i + button.getHeight()/20);
            stage.addActor(button);
            buttons[i] = button;
        }
        buttons[0].getLabel().setText("Check / Call");
        buttons[0].addListener(new InputListener() {
                public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
                    game.call(0);
                    return true;
                }
            });
        buttons[1].getLabel().setText("Bet / Raise");
        buttons[1].addListener(new InputListener() {
                public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
                    game.bet(0,20);
                    return true;
                }
            });
        buttons[2].getLabel().setText("All-In");
        buttons[2].addListener(new InputListener() {
                public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
                    game.allin(0);
                    return true;
                }
            });
	}

	public void render () {
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		stage.act(Gdx.graphics.getDeltaTime());
		stage.draw();
	}

	public void resize (int width, int height) {
		stage.getViewport().update(width, height, true);
	}

	public void dispose () {
		stage.dispose();
    }
    public void resume() {
    }
    public void pause() {
    }
}
