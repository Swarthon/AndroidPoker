package org.swarthon.androidpoker;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Pixmap.Format;
import com.badlogic.gdx.files.FileHandle;

class CardDesign {
    static CardDesign singleton = null;
    static CardDesign getSingleton() {
        if(singleton == null)
            singleton = new CardDesign();
        return singleton;
    }

    Pixmap cardMap;
    Pixmap cardBack;
    FileHandle dataFile = Gdx.files.internal("cards.png");
    FileHandle backFile = Gdx.files.internal("back.png");

    public static final int CARD_WIDTH = 500;
    public static final int CARD_HEIGHT = 700;

    protected static final int PIXMAP_WIDTH = 536;
    protected static final int PIXMAP_HEIGHT = 750;
    
    protected CardDesign() {
        cardMap = new Pixmap(dataFile);
        cardBack = new Pixmap(backFile);
    }
    public Pixmap getCardDesign(Card c) {
        Card.Color color = c.getColor();
        Card.Number number = c.getNumber();
        
        Pixmap pixmap = new Pixmap(CARD_WIDTH, CARD_HEIGHT, Format.RGBA8888);
        pixmap.drawPixmap(cardMap,
                          number.ordinal() * PIXMAP_WIDTH,
                          color.ordinal() * PIXMAP_HEIGHT,
                          PIXMAP_WIDTH, PIXMAP_HEIGHT,
                          0, 0,
                          CARD_WIDTH, CARD_HEIGHT);
        return pixmap;
    }
    public Pixmap getCardBack() {
        return cardBack;
    }
}
