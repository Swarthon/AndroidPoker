package org.swarthon.androidpoker;

import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.utils.DragListener;

class CardObject extends Image {
    Card card = null;
    CardObject() {
        super(createSkin(), "Card");
        setSize(CardDesign.CARD_WIDTH/2, CardDesign.CARD_HEIGHT/2);
    }
    CardObject(Card c) {
        super(createSkin(c), "Card");
        setSize(CardDesign.CARD_WIDTH/2, CardDesign.CARD_HEIGHT/2);
        card = c;
    }

    void setBack() {
        setDrawable(createSkin(),"Card");
    }
    void update(Card card) {
        setDrawable(createSkin(card),"Card");
    }
    protected static Skin createSkin() {
        final Skin skin = new Skin();
        final Pixmap pixmap = CardDesign.getSingleton().getCardBack();
        skin.add("Card", new Texture(pixmap));
        return skin;
    }
    protected static Skin createSkin(Card c) {
        final Skin skin = new Skin();
        final Pixmap pixmap = CardDesign.getSingleton().getCardDesign(c);
        skin.add("Card", new Texture(pixmap));
        pixmap.dispose();
        return skin;
    }
}
