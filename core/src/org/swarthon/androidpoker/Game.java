package org.swarthon.androidpoker;

import java.util.Vector;
import java.util.Random;
import java.util.Collections;
import java.util.concurrent.ThreadLocalRandom;

class Game {
    PlayerObject[] players;
    Card[] center;
    CardObject[] centerObjects;
    Vector<Card> deck = new Vector();
    int pot = 0;
    int bet = INITIAL_BET;
    int actualPlayer = 0;
    int innerTurn = 0;
    boolean skipTurn = true;

    final static int INITIAL_BET = 20;

    Game(int numPlayers, int money) {
        for(Card.Color c : Card.Color.values())
            for(Card.Number n : Card.Number.values())
                deck.add(new Card(c,n));
        shuffle(deck);

        players = new PlayerObject[numPlayers];
        for(int i = 0; i < players.length; i++) {
            Player p = new Player(draw(), draw(), money);
            if(i != 0)
                players[i] = new PlayerObject(p, false);
            else
                players[i] = new PlayerObject(p, true);
        }

        center = new Card[5];
        centerObjects = new CardObject[5];
        for(int i = 0; i < center.length; i++) {
            center[i] = draw();
            if(i > 2)
                centerObjects[i] = new CardObject();
            else
                centerObjects[i] = new CardObject(center[i]);
        }
    }
    void rebuildDeck() {
        deck.clear();
        for(Card.Color c : Card.Color.values())
            for(Card.Number n : Card.Number.values())
                deck.add(new Card(c,n));
        shuffle(deck);
    }
    void redrawCards() {
        for(PlayerObject p : players) {
            p.getPlayer().setCards(draw(),draw());
            p.update();
        }
        for(CardObject c : centerObjects) {
            
        }
        for(int i = 0; i < center.length; i++) {
            center[i] = draw();
            if(i > 2)
                centerObjects[i].setBack();
            else
                centerObjects[i].update(center[i]);
        }
    }

    
    // Getter
    Vector<Card> getDeck() {
        return deck;
    }
    PlayerObject[] getPlayers() {
        return players;
    }
    CardObject[] getCenter() {
        return centerObjects;
    }
    int getPot() {
        return pot;
    }
    int getBet() {
        return bet;
    }
    Card draw() {
        return deck.remove(0); 
    }


    public static <T> void shuffle(Vector<T> vec) {
        Random rnd = ThreadLocalRandom.current();
        for (int i = vec.size() - 1; i > 0; i--) {
            int index = rnd.nextInt(i + 1);
            Collections.swap(vec,index,i);
        }
    }


    void nextPlayer() {
        actualPlayer++;
        if(actualPlayer >= players.length) {
            actualPlayer = 0;
            if(skipTurn)
                increaseInnerTurn();
            else
                skipTurn = true;
        }
        if(actualPlayer != 0)
            call(actualPlayer);
    }
    void increaseInnerTurn() {
        innerTurn++;
        if(innerTurn == 1 || innerTurn == 2) {
            centerObjects[2+innerTurn].update(center[2+innerTurn]);
        }
        if(innerTurn == 3) {
            innerTurn = 0;
            pot = 0;
            bet = INITIAL_BET;
            actualPlayer = 0;
            skipTurn = true;
            win();
        }
    }
    void win() {
        rebuildDeck();
        redrawCards();
    }
    
    // Gamble
    public void call(int i) {
        if(players[i].getPlayer().getMoney() - bet > 0) {
            pot += bet;
            players[i].getPlayer().pay(bet);
            nextPlayer();
        }
        else
            allin(i);
    }
    public void bet(int i, int sum) {
        if(players[i].getPlayer().getMoney() - sum - bet > 0) {
            bet += sum;
            pot += bet;
            players[i].getPlayer().pay(bet);
            skipTurn = false;
            nextPlayer();
        }
        else
            allin(i);
    }
    public void allin(int i) {
        bet = players[i].getPlayer().getMoney();
        pot += bet;
        players[i].getPlayer().pay(bet);
        nextPlayer();
    }
}
