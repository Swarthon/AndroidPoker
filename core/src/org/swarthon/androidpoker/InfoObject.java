package org.swarthon.androidpoker;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;

class InfoObject extends Group {
    Label pot;
    Label bet;

    Game game;

    InfoObject(Game g) {
        super();

        game = g;
        
        final Skin skin = new Skin();
        skin.add("default", new LabelStyle(new BitmapFont(), Color.WHITE));
        
        pot = new Label(String.valueOf(game.getPot()) + " $", skin);
        pot.setFontScale(3);
        pot.setPosition(getWidth()/2 - pot.getWidth()/2, pot.getHeight());
        addActor(pot);

        bet = new Label(String.valueOf(game.getBet()) + " $", skin);
        bet.setFontScale(3);
        bet.setPosition(getWidth()/2 - bet.getWidth()/2, -2 * bet.getHeight());
        addActor(bet);

    }
    
    @Override
    public void act(float delta) {
        pot.setText("Pot : " + String.valueOf(game.getPot()) + " $");
        bet.setText("Bet : " + String.valueOf(game.getBet()) + " $");
    }
}
