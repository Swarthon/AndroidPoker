package org.swarthon.androidpoker;

class Player {
    int money;
    Card cards[];

    Player(Card one, Card two, int m) {
        cards = new Card[2];
        cards[0] = one;
        cards[1] = two;
        money = m;
    }

    public Card[] getCards() {
        return cards;
    }
    public void setCards(Card one, Card two) {
        cards[0] = one;
        cards[1] = two;
    }
    public void setCards(Card[] c) {
        cards = c;
    }
    public int getMoney() {
        return money;
    }
    public void pay(int s) {
        money -= s;
    }
}
