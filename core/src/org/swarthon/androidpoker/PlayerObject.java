package org.swarthon.androidpoker;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;

class PlayerObject extends Group {
    CardObject cards[];
    Label label;
    Player player;
    Boolean show;

    PlayerObject(Player p, Boolean b) {
        super();
        player = p;
        show = b;

        int size = player.getCards().length;
        cards = new CardObject[size];
        for(int i = 0; i < size; i++) {
            if(show)
                cards[i] = new CardObject(player.getCards()[i]);
            else
                cards[i] = new CardObject();
            cards[i].setPosition((1.f/2.f * (float)i - 3.f/8.f * (float)size) * CardDesign.CARD_WIDTH / 2.f,
                          - CardDesign.CARD_HEIGHT/4);
            addActor(cards[i]);
        }

        final Skin skin = new Skin();
        skin.add("default", new LabelStyle(new BitmapFont(), Color.WHITE));
        
        label = new Label(String.valueOf(player.getMoney()) + " $", skin);
        label.setFontScale(3);
        label.setPosition(getWidth()/2 - label.getWidth()/2,
                          - CardDesign.CARD_HEIGHT/8 * 3 + label.getHeight());
        addActor(label);
    }

    Player getPlayer() {
        return player;
    }

    void update() {
        for(int i = 0; i < cards.length; i++) {
            if(show)
                cards[i].update(player.getCards()[i]);
            else
                cards[i].setBack();
        }
    }
    @Override
    public void act(float delta) {
        label.setText(String.valueOf(player.getMoney()) + " $");
    }
}
